/*-----------------------------------------------------------------------------------------------------------------------|
| MIT License (MIT)                                                                                                      |
| Copyright (C) 2017 by Michel Braz de Morais  <michel.braz.morais@gmail.com>                                            |
|                                                                                                                        |
| Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated           |
| documentation files (the "Software"), to deal in the Software without restriction, including without limitation        |
| the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and       |
| to permit persons to whom the Software is furnished to do so, subject to the following conditions:                     |
|                                                                                                                        |
| The above copyright notice and this permission notice shall be included in all copies or substantial portions of       |
| the Software.                                                                                                          |
|                                                                                                                        |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE   |
| WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR  |
| COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       |
| OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
|------------------------------------------------------------------------------------------------------------------------|
|  Example of use the lib plusWindows for owner-drawn of buttons, labels, radio, checkbox ... etc                        |
|-----------------------------------------------------------------------------------------------------------------------*/


#ifdef __MINGW32__
	#define IDI_ICON1 0 
#elseif VS_2008_EXPRES
	#define IDI_ICON1 0 
	#define USE_THEME_WINDOWS	
#else
	#include "resource.h"
	#define USE_THEME_WINDOWS	
#endif

#include "src/plusWindows/plusWindows.h"
#include "src/plusWindows/defaultThemePlusWindows.h"
#include "src/renderer/brazil-flag.h"

using namespace mbm;

void oneSecond(WINDOW* w, DATA_EVENT &dataEvent)
{
    if(dataEvent.userDrawer && dataEvent.userDrawer->that)
    {
        static int second = 0;
        if(second++ == 60)
            second = 0;
	    char str[1024];
	    sprintf(str, "Seconds: %d", second);
	    int * idStatusBar = static_cast<int*>(dataEvent.userDrawer->that);
        w->setText(*idStatusBar, str, 1);
    }
}


void onClick(WINDOW *w,DATA_EVENT &dataEvent)
{
    int* idEventsLabel = static_cast<int*>(w->getObjectContext(1));
    char str[255]="";
    switch(dataEvent.type)
    {
        case WINPLUS_TYPE_NONE               :{sprintf(str,"Event: NONE                \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_WINDOW             :{sprintf(str,"Event: WINDOW              \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_LABEL              :{sprintf(str,"Event: LABEL               \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_BUTTON             :{sprintf(str,"Event: BUTTON              \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_BUTTON_TAB         :{sprintf(str,"Event: BUTTON_TAB          \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_CHECK_BOX          :{sprintf(str,"Event: CHECK_BOX           \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_RADIO_BOX          :{sprintf(str,"Event: RADIO_BOX           \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_COMBO_BOX          :{sprintf(str,"Event: COMBO_BOX           \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_LIST_BOX           :{sprintf(str,"Event: LIST_BOX            \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_TEXT_BOX           :{sprintf(str,"Event: TEXT_BOX            \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_SCROLL             :{sprintf(str,"Event: SCROLL              \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_SPIN_INT           :{sprintf(str,"Event: SPIN_INT            \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_SPIN_FLOAT         :{sprintf(str,"Event: SPIN_FLOAT          \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_RICH_TEXT          :{sprintf(str,"Event: RICH_TEXT           \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_CHILD_WINDOW       :{sprintf(str,"Event: CHILD_WINDOW        \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_GROUP_BOX          :{sprintf(str,"Event: GROUP_BOX           \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_PROGRESS_BAR       :{sprintf(str,"Event: PROGRESS_BAR        \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_TIMER              :{sprintf(str,"Event: TIMER               \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_TRACK_BAR          :{sprintf(str,"Event: TRACK_BAR           \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_STATUS_BAR         :{sprintf(str,"Event: STATUS_BAR          \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_MENU               :{sprintf(str,"Event: MENU                \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_SUB_MENU           :{sprintf(str,"Event: SUB_MENU            \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_GROUP_BOX_TAB      :{sprintf(str,"Event: GROUP_BOX_TAB       \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_TRY_ICON_MENU      :{sprintf(str,"Event: TRY_ICON_MENU       \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_TRY_ICON_SUB_MENU  :{sprintf(str,"Event: TRY_ICON_SUB_MENU   \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_TOOL_TIP           :{sprintf(str,"Event: TOOL_TIP            \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_WINDOWNC           :{sprintf(str,"Event: WINDOWNC            \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_WINDOW_MESSAGE_BOX :{sprintf(str,"Event: WINDOW_MESSAGE_BOX  \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
        case WINPLUS_TYPE_IMAGE              :{sprintf(str,"Event: IMAGE               \nid:[%d] value:[%s]",dataEvent.idComponent,dataEvent.getAsString());}break;
    }
    w->setText(*idEventsLabel,str);
}

struct USER_THEME : public USER_DRAWER
{
    std::map<int,THEME_WINPLUS_CUSTOM_RENDER*> themes;
    std::map<int,THEME_WINPLUS_CUSTOM_RENDER*> oldThemes;
    int lastIndex;
    bool lastBorder;
    BRAZIL_FLAG* brazilFlag;
    USER_THEME(BRAZIL_FLAG* BrazilFlag)
    {
        brazilFlag = BrazilFlag;
        lastIndex = 11;
        lastBorder = true;
    }
    virtual ~USER_THEME()
    {
        for (auto & i: themes  )
        {
            delete i.second;
        }

        for (auto & i: oldThemes  )
        {
            delete i.second;
        }
    }
    virtual bool      render(COMPONENT_INFO &)
    {
        return false;
    }
    void setTheme(WINDOW* w,int indexTheme, bool border)
    {
        THEME_WINPLUS_CUSTOM_RENDER * theme = themes[indexTheme];
        if(theme)
        {
            THEME_WINPLUS_CUSTOM_RENDER * old = oldThemes[indexTheme];
            if(old)
                delete old;
            oldThemes[indexTheme] = theme;
        }
        theme = new THEME_WINPLUS_CUSTOM_RENDER();
        theme->setTheme(indexTheme,border);
        themes[indexTheme] = theme;
        w->setTheme(theme);
        //we need to restore the flag drawer
        w->setDrawer(brazilFlag,brazilFlag->idgroup);
        lastIndex = indexTheme;
        lastBorder = border;
    }
};



void onSelectTheme(WINDOW* w, DATA_EVENT & data)
{
    if(data.userDrawer && data.userDrawer->that)
    {
        USER_THEME*  userTheme = static_cast<USER_THEME*>(data.userDrawer->that);
        int selectedIndex = data.getAsInt();
        userTheme->setTheme(w,selectedIndex,userTheme->lastBorder);
    }
}

void onSelectBorder(WINDOW* w, DATA_EVENT & data)
{
    if(data.userDrawer && data.userDrawer->that)
    {
	    bool border = data.getAsBool();
        USER_THEME*  userTheme = static_cast<USER_THEME*>(data.userDrawer->that);
        userTheme->setTheme(w,userTheme->lastIndex,border);
    }
}

int main()
{
	WINDOW w;
    
	BMP bmp;
	w.setObjectContext(&bmp,0);//userdata index 0
    

    //init our window
	w.init("plusWindows Exemple",600, 600,0,0,0,false,true,false,nullptr,false,IDI_ICON1,true);//using double buffer

	//Add menu to window
	int menu1 = w.addMenu("menu 1", onClick);
	w.addSubMenu("Menu 1", menu1);
	w.addSubMenu("Menu 2", menu1);
	w.addSubMenu(nullptr, menu1);//separator
	w.addSubMenu("Menu 3", menu1);
    w.addSubMenu("Other Menu",menu1);

	int idGb = w.addGroupBox("Group box", 10, 50, 275, 300);
	w.addLabel("Label", 10, 25, 100, 20, idGb);
	w.addRadioButton("Radio b. 1", 10, 50, 100, 20, idGb, onClick);
	w.addRadioButton("Radio b. 2", 10, 75, 100, 20, idGb, onClick);
	w.addCheckBox("Check box 1", 10, 100, 100, 20, onClick, idGb);
	w.addCheckBox("Check box 2", 10, 125, 100, 20, onClick, idGb);

	int idCombo = w.addCombobox(10, 150, 100, 20, onClick, idGb);
	w.addText(idCombo, "comboBox 1");
	w.addText(idCombo, "comboBox 2");
	w.addText(idCombo, "comboBox 3");
	w.addText(idCombo, "comboBox 4");
    w.setSelectedIndex(idCombo,0);

	int idList = w.addListBox(10, 200, 100, 20, onClick, idGb);
	w.addText(idList, "ListBox 1");
	w.addText(idList, "ListBox 2");
	w.addText(idList, "ListBox 3");
	w.addText(idList, "ListBox 4");

	//group box 1
	w.addButton("Button", 125, 25, 125, 20,idGb, onClick);
	w.addSpinInt(125, 50, 100, 20, idGb,0,0,onClick);
	w.addSpinFloat(125, 75, 100, 20, idGb,0,0,0.0f,100.0f,0.5f,1.0f,2,true,true,onClick);
	w.addProgressBar(125, 100, 125, 20, idGb);
	w.addTextBox("Text box", 125, 125, 125, 20, idGb);
	w.addRichText("Line 1\r\nLine 2", 125, 150, 125, 125, idGb);

	idGb = w.addGroupBox("Theme", 10, 375, 275, 75);
	USER_THEME userTheme(nullptr);
    int idTheme = w.addCombobox(10,25,200,20,onSelectTheme, idGb,&userTheme);
    for(int i=0; i < THEME_WINPLUS_CUSTOM_RENDER::getTotalThemes(); ++i)
    {
        char str[255];
        sprintf(str,"Theme - %2d",i+1);
        w.addText(idTheme,str);
    }
    w.setSelectedIndex(idTheme,11);
	int idBorder = w.addCheckBox("Border", 10, 50, 200, 20,onSelectBorder,idGb,&userTheme);
    w.setCheckBox(true,idBorder);

	int idTabCntrl = w.addTabControlByGroup(300, 30, 275, 300, 50, 20);
	int tab = w.addTabByGroup("Tab 1", idTabCntrl);
	w.addTextBox("Text box", 25, 25, 200, 20, tab);
	w.addTrackBar(25, 50, 200, 50, tab);
	w.addTrackBar(25, 100, 50, 150, tab, nullptr, 0.0f, 100.0f, 70.0f, 10.0f,25.0f,true,true);

	tab = w.addTabByGroup("Tab 2", idTabCntrl);
	w.addRichText("Rich text editor\r\nLine 2\r\nLine 3\r\nLine 4\r\nLine 5\r\n...", 25, 25, 225, 250,tab);
	w.setIndexTabByGroup(idTabCntrl, 0);
    int idStatusBar = w.addStatusBar("Status bar",2);

    USER_DATA userData(&idStatusBar);
	w.addTimer(1000, oneSecond,&userData);//each second
	
	w.addTrayIcon(onClick,"Title");
	w.addMenuTrayIcon("Menu 1");
	int subMenu = w.addSubMenuTrayIcon("Menu 2 Plus Sub-Menu");
	w.addMenuTrayIcon("Sub-Menu 1",subMenu);
	w.addMenuTrayIcon("Sub-Menu 2",subMenu);
	subMenu = w.addSubMenuTrayIcon("Menu 3 Plus Sub-Menu");
	w.addMenuTrayIcon("Sub-Menu 3",subMenu);
	w.hideConsoleWindow();//hide console
	int idgroupFlagBrazil = w.addGroupBox("",300,375,275,150);
    int idEventsLabel = w.addLabel("Events:",25,475,200,100);
    w.setObjectContext(&idEventsLabel,1);//userdata
	
    //specific renderer
    BRAZIL_FLAG brazilFlag(&w,idgroupFlagBrazil);
    userTheme.brazilFlag = &brazilFlag;
    userTheme.that = &userTheme;
	w.setDrawer(&brazilFlag,idgroupFlagBrazil);
    w.setObjectContext(&brazilFlag,2);//userdata
	w.enterLoop(nullptr);

	return 0;
}

