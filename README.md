# PlusWindows library for Win32

The plusWindows library is a simple library for creating Owner-Drawn components on Windows.
It has custom Owner-Drawn Buttons, Radio-box, Check-box, List-box, Text-box, Group-box, Combo-box, Status-Bar, Custom-Window and Menu.
It has 23 themes Owner-Drawn until now.

## Links

* Project Page - https://bitbucket.org/Michel-braz-morais/pluswindows


## Introduction

For easily creating windows and components using win32 environment under C++. It Has 2 sources to build (plusWindows.h and defaultThemePlusWindows.h).
Also has native calls using win32 for tray icon, timer and Bitmap reader, writer and draw.

## Authors

  - Michel Braz de Morais <michel.braz.morais@gmail.com> - Creation and maintenance of plusWindows library
  - Iraja  Eric           <irajaeric@gmail.com>          - Creation of convolucao app using plusWindows.
  - Adao                                                 - Creation of snake++ app using plusWindows.

## Examples

### Hello world

```cpp
#include <plusWindows.h>

using namespace mbm;

//id global for demo proposal
int idTextBox = -1;//uninitialized id

void onClickButton(WINDOW* w, DATA_EVENT & data)
{
    char name[255];
    if(w->getText(idTextBox,name,sizeof(name)))
        w->messageBox("Hello! you have pressed [%s] \nYour name:\n%s",data.getAsString(),name);
    //quit application
    w->run = false;
}


int main(int argc,char** argv)
{
    WINDOW window;

    //init our window 300x200 (width and height)
    window.init("Hello World",300,200);

    //add label "Input your name:" at x=10, y=10, width=200, height=20
    window.addLabel("Input your name:",10,10,200,20);

    //add text-box filled "My name here" at x=10, y=40, width=200, height=20
    //add the id to global for demo proposal
    idTextBox = window.addTextBox("My name here",10,40,200,20);

    //add a button action at x=10, y=80, width=40, height=20, -1 means to window, onClickButton is the callback when clicked
    window.addButton("Ok",10,80,40,20,-1,onClickButton);

    //enter loop and wait for events
    return window.enterLoop(nullptr);
}

```

#### Hello-world
![Scheme](images/hello-world-demo-1.png)

![Scheme](images/hello-world-demo-2.png)

![Scheme](images/hello-world-demo-3.png)

#### Convolucao - thanks Iraja
![Scheme](images/convolucao-by-iraja.png)

#### Snake++ - thanks Adao
![Scheme](images/Snake-by-Adao.png)

#### PlusWindows components 
Rendering Brazil's flag at runtime
![Scheme](images/plusWindows-components.png)


### Available themes

You can easily change the theme before init the main application.
> For now there are 23 themes availables.
> You just choose it by id: (0-23)

```cpp
int main(int argc,char** argv)
{
	//set the theme
	THEME_WINPLUS_CUSTOM_RENDER::setTheme(0,true);
	
    WINDOW window;
	
    //init our window 800x600 
    window.init("Hello World",300,200);
	...
}
```
### Some themes
#### Theme id: 1
![Scheme](images/theme-0.png)

#### Theme id: 2
![Scheme](images/theme-2.png)

#### Theme id: 3
![Scheme](images/theme-3.png)

#### Theme id: 4
![Scheme](images/theme-4.png)

#### Theme id: 16
![Scheme](images/theme-16.png)

#### Theme id: 18
![Scheme](images/theme-18.png)

#### Theme id: 20
![Scheme](images/theme-20.png)

#### Theme id: 22
![Scheme](images/theme-22.png)

## License

PlusWindows is under MIT License (MIT) and the proposal is creating Owner-Drawn components on Windows (Win32).

The MIT License (MIT)

Copyright (c) 2017 [Michel Braz de Morais](michel.braz.morais@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

        
          