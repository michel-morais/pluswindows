/*-----------------------------------------------------------------------------------------------------------------------|
| MIT License (MIT)                                                                                                      |
| Copyright (C) 2015      by Michel Braz de Morais  <michel.braz.morais@gmail.com>                                       |
|                                                                                                                        |
| Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated           |
| documentation files (the "Software"), to deal in the Software without restriction, including without limitation        |
| the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and       |
| to permit persons to whom the Software is furnished to do so, subject to the following conditions:                     |
|                                                                                                                        |
| The above copyright notice and this permission notice shall be included in all copies or substantial portions of       |
| the Software.                                                                                                          |
|                                                                                                                        |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE   |
| WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR  |
| COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       |
| OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
|                                                                                                                        |
|-----------------------------------------------------------------------------------------------------------------------*/

#include <plusWindows.h>
#include <defaultThemePlusWindows.h>

using namespace mbm;

//id global for demo proposal
int idTextBox = -1;//uninitialized id

void onClickButton(WINDOW* w, DATA_EVENT & data)
{
    char name[255];
    if(w->getText(idTextBox,name,sizeof(name)))
        w->messageBox("Hello! you have pressed [%s] \nYour name:\n%s",data.getAsString(),name);
    //quit application
    w->run = false;
}


int main(int argc,char** argv)
{
    WINDOW window;

    //select theme
    THEME_WINPLUS_CUSTOM_RENDER::setTheme(11,true);

    //init our window 300x200 
    window.init("Hello World",300,200);

    //add label "Input your name:" at x=10, y=10, width=200, height=20
    window.addLabel("Input your name:",10,10,200,20);

    //add text-box filled "My name here" at x=10, y=40, width=200, height=20
    //add the id to global for demo proposal
    idTextBox = window.addTextBox("My name here",10,40,200,20);

    //add a button action at x=10, y=80, width=40, height=20, -1 means to window, onClickButton is the callback when clicked
    window.addButton("Ok",10,80,40,20,-1,onClickButton);

    //hide console window
    window.hideConsoleWindow();

    //enter loop and wait for events
    return window.enterLoop(nullptr);
}
