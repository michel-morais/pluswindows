/*-----------------------------------------------------------------------------------------------------------------------|
| MIT License (MIT)                                                                                                      |
| by Adao and refactored by michel                                                                                       |
| Thanks Adao! I just refactored some functions however the code is pretty much the same!                                |
|                                                                                                                        |
| Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated           |
| documentation files (the "Software"), to deal in the Software without restriction, including without limitation        |
| the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and       |
| to permit persons to whom the Software is furnished to do so, subject to the following conditions:                     |
|                                                                                                                        |
| The above copyright notice and this permission notice shall be included in all copies or substantial portions of       |
| the Software.                                                                                                          |
|                                                                                                                        |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE   |
| WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR  |
| COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       |
| OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
|------------------------------------------------------------------------------------------------------------------------|
|  Snake game using plusWindows                                                                                          |
|-----------------------------------------------------------------------------------------------------------------------*/

#include "snake.h"

TIME_CONTROL::TIME_CONTROL():
    fps(0),
    timeCntRunTime					(0),
    fpsProp(0.0f),
    timeCntCurrentTickCount         (0),
    initialTime						(0),
    lastTickCount					(GetTickCount()),
    currentRate						(15),
    countFps						(0),
	rateToUpdateFps					(500)
    {}

char* TIME_CONTROL::getAsMinute()
{
	unsigned int sec = (unsigned int)(timeCntRunTime);
	unsigned int min = (unsigned int)((float)sec/60.0f);
	sec = (unsigned int )(timeCntRunTime - ((float)min * 60.0f));
	static char str[255];
	sprintf(str,"%02u:%02u",min,sec);
	return str;
}

unsigned int TIME_CONTROL::add(const float value)
{	
	aditionalTimes.push_back(value);
	return aditionalTimes.size() -1;
}
	
float TIME_CONTROL::get(const unsigned int index)const
{	
	if(index >= aditionalTimes.size())
		return 0.0f;
	return aditionalTimes[index];
}
	
bool TIME_CONTROL::set(const unsigned int index,const float newTime)
{	
	if(index >= aditionalTimes.size())
		return false;
	aditionalTimes[index] = newTime;
	return true;
}
	
void TIME_CONTROL::updateFps()
{	
	timeCntCurrentTickCount = GetTickCount();
	const float timeSecNow=(0.001f*(timeCntCurrentTickCount - lastTickCount));
	timeCntRunTime+=timeSecNow;
	for(unsigned int x = 0;x < aditionalTimes.size(); ++x)
	{	
		aditionalTimes[x]	+=	timeSecNow;
	}
	lastTickCount		=	timeCntCurrentTickCount;
	float currentTimeMs	=	timeCntCurrentTickCount*0.001f;
	countFps++;
	if(rateToUpdateFps>1000)
		rateToUpdateFps=1000;
	if(rateToUpdateFps<50)
		rateToUpdateFps=50;
	float x=(float)((float)rateToUpdateFps * 0.001f);
	if((currentTimeMs - initialTime) > x)
	{	
		float y=1.0f/x;
		currentRate=(unsigned int)(countFps * y);
		initialTime = currentTimeMs;
		countFps=0;
	}
	fps=((unsigned int) (currentRate));
	fpsProp = 1.0f/(float)fps;
}
	

BROWSE::BROWSE(DIRECTION direction,int posX,int posY)
{
	dir	=	direction;
	x	=	posX;
	y	=	posY;
}
BROWSE::BROWSE()
{
	dir	=	DIR_TO_R;
	x	=	0;
	y	=	0;
}


SNAKE::SNAKE(	unsigned char r,
		unsigned char g, 
		unsigned char b,
		POINT* foodExtern,
		const int initialPositionX,
		const int initialPositionY,
        const int WPixel,
        mbm::DRAW* draw)
{
	widthRect = 0;
	heightRect = 0;
	initialSpeed	=	0.27f;
	incrementSpeed	=	0.005f;
	speed			=	initialSpeed;
	minimumSpeed	=	0.02f;
	idTimerWalkSnake = 	0xffffffff;
    currentFirst	=	0;
	wPixel			=	WPixel;
	eatedFood		=	false;
	isGameOver		=	false;
	food			=	foodExtern;
	initialPosX		=	initialPositionX;
	initialPosY		=	initialPositionY;
	memset(&bound,0,sizeof(RECT));
	memset(strBrowes,0,sizeof(strBrowes));

    blackBrushSnake = draw->createBrush(r,g,b);
    penBorderSnake = draw->createPen(255,100,0);

    brushBrowse = draw->createBrush(255,100,0);
    penBrowse   = draw->createPen(255,100,0);
}

SNAKE::~SNAKE()
{
	this->release();
    DeleteObject(blackBrushSnake);
    DeleteObject(penBorderSnake);

    DeleteObject(brushBrowse);
    DeleteObject(penBrowse);
}

void SNAKE::release()
{
	for(unsigned int i=0; i< lsBrowse.size(); i++)
	{
		BROWSE*	ptr	=	lsBrowse[i];
		if(ptr)
			delete ptr;
		lsBrowse[i]	=	NULL;
	}
	lsBrowse.clear();
	currentFirst	=	0;
	eatedFood		=	false;
}
	
void SNAKE::restart(int w,int h,TIME_CONTROL *timer,int margin,int infoBottom)
{
	this->release();
    speed					=	initialSpeed;
    idTimerWalkSnake 		= 	timer->add();
    bound.top               =   margin;
    bound.left              =   margin;
    bound.right			    =	w - margin;
	bound.bottom		    =	h - infoBottom - margin;

    widthRect               =   bound.right - bound.left;
    heightRect              =   bound.bottom - bound.top;
	//initial size 3 
    addBrowse();
	addBrowse();
	addBrowse();
	food->x					=	mbm::getRandomInt(1, widthRect  - (wPixel)) + bound.left;
	food->y					=	mbm::getRandomInt(1, heightRect - (wPixel)) + bound.top;
	isGameOver				=	false;
}

const unsigned int SNAKE::getSize()const
{
	return lsBrowse.size();
}
	
void SNAKE::render(DRAW* draw)
{
    draw->setPen(this->penBorderSnake);
    draw->setBrush(this->blackBrushSnake);
	const unsigned int s = lsBrowse.size();
	for(unsigned int i=0; i< s; i++)
	{
		BROWSE*	browse	=	lsBrowse[i];
		draw->drawRectangle(browse->x,browse->y,wPixel,wPixel);
	}
    draw->setPen(this->penBrowse);
    draw->setBrush(this->brushBrowse);
	draw->drawRectangle(food->x,food->y,wPixel,wPixel);
}

bool SNAKE::move()
{
	const unsigned int s = lsBrowse.size();
	if(!s || isGameOver)
	{
		currentFirst	=	0;
		return false;
	}
	if(eatedFood)
	{
		addBrowse();
		food->x					=	mbm::getRandomInt(1, widthRect  - (wPixel)) + bound.left;
	    food->y					=	mbm::getRandomInt(1, heightRect - (wPixel)) + bound.top;
		speed					-=	incrementSpeed;
		if(speed < minimumSpeed)
			speed	=	minimumSpeed;
		eatedFood				=	false;
	}
	if(currentFirst >= this->lsBrowse.size())
		currentFirst = 0;
	BROWSE*	first	=	lsBrowse[currentFirst];
	BROWSE*	last	=	NULL;
	BROWSE  browse;
	unsigned int lastCurrent = 0;
	if(currentFirst == 0)
		lastCurrent = lsBrowse.size() -1;
	else
		lastCurrent = currentFirst-1;
	last = lsBrowse[lastCurrent];
	switch(last->dir)
	{
		case DIR_STOPED:
		{
			return false;
		}
		break;
		case DIR_TO_R:
		{
			browse.x		=	last->x + (wPixel);
			browse.y		=	last->y;
			browse.dir		=	DIR_TO_R;
		}
		break;
		case DIR_TO_L:
		{
			browse.x		=	last->x - wPixel;
			browse.y		=	last->y;
			browse.dir		=	DIR_TO_L;
		}
		break;
		case DIR_TO_U:
		{
			browse.x		=	last->x;
			browse.y		=	last->y - wPixel;
			browse.dir		=	DIR_TO_U;
		}
		break;
		case DIR_TO_D:
		{
			browse.x		=	last->x;
			browse.y		=	last->y + (wPixel);
			browse.dir		=	DIR_TO_D;
		}
		break;
			
	}
	first->x	=	browse.x;
	first->y	=	browse.y;
	first->dir	=	browse.dir;
	if(checkColision())
	{
		this->lsBrowse.erase(this->lsBrowse.begin() + lastCurrent);
		if(this->lsBrowse.size() <=1)
			isGameOver = true;
		delete last;
		sprintf(strBrowes,"Size: %u",lsBrowse.size());
	}
	else
	{
		currentFirst++;
		if(currentFirst >= this->lsBrowse.size())
			currentFirst = 0;
		//printf("\nmoveu x %d y %d",browse.x,browse.y);
	}
	if(eatFood())
	{
		eatedFood	=	true;
	}
    return true;
}
	
void SNAKE::addBrowse()//adiciona um gomo --OK
{
	if(currentFirst < lsBrowse.size())
	{
		BROWSE*	last	=	NULL;
		if(currentFirst == 0)
			last = lsBrowse[lsBrowse.size() -1];
		else
			last = lsBrowse[currentFirst -1];
		
		BROWSE*	browse 	= new BROWSE(last->dir,0,0);
		switch(last->dir)
		{
			case DIR_STOPED:
			case DIR_TO_R:
			{
				browse->x	=	last->x + (wPixel)+1;
				browse->y	=	last->y;
			}
			break;
			case DIR_TO_L:
			{
				browse->x	=	last->x - wPixel-1;
				browse->y	=	last->y;
			}
			break;
			case DIR_TO_U:
			{
				browse->x	=	last->x;
				browse->y	=	last->y - wPixel-1;
			}
			break;
			case DIR_TO_D:
			{
				browse->x	=	last->x;
				browse->y	=	last->y + wPixel+1;
			}
			break;
		}
		lsBrowse.push_back(browse);
		sprintf(strBrowes,"size: %u",lsBrowse.size());
	}
	else
	{
		BROWSE*	browse 	= new BROWSE(DIR_STOPED,initialPosX,initialPosY);
		lsBrowse.push_back(browse);
	}
}
	
void SNAKE::set(DIRECTION dir)
{
	if(lsBrowse.size())
	{
		BROWSE*	last	=	NULL;
		if(currentFirst == 0)
			last = lsBrowse[lsBrowse.size() -1];
		else
			last = lsBrowse[currentFirst -1];
			
		switch(dir)
		{
			case DIR_TO_R:
			{
				if(last->dir == DIR_TO_L)
					return;
			}
			break;
			case DIR_TO_L:
			{
				if(last->dir == DIR_TO_R)
					return;
			}
			break;
			case DIR_TO_U:
			{
				if(last->dir == DIR_TO_D)
					return;
			}
			break;
			case DIR_TO_D:
			{
				if(last->dir == DIR_TO_U)
					return;
			}
			break;
			default:
				return;
		}
		last->dir = dir;
	}
}
	
bool SNAKE::checkColision()
{
	for(unsigned int i=0; i< lsBrowse.size(); i++)
	{
		for(unsigned int j=0; j< lsBrowse.size(); j++)
		{
			if(i != j)
			{
				BROWSE*	browse	=	lsBrowse[i];
				BROWSE*	other	=	lsBrowse[j];
				if(collided(browse,other))
				{
					printf("\nCollision i %u j %u",i,j);
					return true;
				}
			}
		}
	}
	return false;
}
	
bool SNAKE::collided(BROWSE* browse,BROWSE*	other)
{
	if(browse == other)
		return false;
	if((browse->x + wPixel) > (widthRect + bound.left))
	{
		return true;
	}
	if((browse->y + wPixel) > (heightRect + bound.top))
	{
		return true;
	}
	if(browse->x < bound.left)
	{
		return true;
	}
	if(browse->y < bound.top)
	{
		return true;
	}
	if(browse->x == other->x && browse->y == other->y)
	{
		return true;
	}
	return false;
}
	
bool SNAKE::isCollededWithFood(BROWSE*	browse)
{
	if(browse->x > (food->x + wPixel))
		return false;
	if((browse->x + wPixel) < food->x)
		return false;
	if(browse->y > (food->y + wPixel))
		return false;
	if((browse->y + wPixel) < food->y)
		return false;
	return true;
}
	
bool SNAKE::eatFood()
{
	for(unsigned int i=0; i< lsBrowse.size(); i++)
	{
		BROWSE*	browse	=	lsBrowse[i];
		if(isCollededWithFood(browse))
			return true;
	}
	return false;
}
	

SNAKE_APP::SNAKE_APP(int sizeBlockSnake)
{
    SNAKE* snake	= new 	SNAKE(255,255,255,&food,20,20,sizeBlockSnake, this);
	lsSnake.push_back(snake);
	snake			= new 	SNAKE(0,0,0,&food,20,100,sizeBlockSnake,this);
	lsSnake.push_back(snake);
	players			=	1;
	widthScreen		=	500;
	heightScreen	=	500;
    blackBrushGame  = this->createBrush(50,50,50);
    blackBrushBorder = this->createBrush(20,20,20);
    penBorder       = this->createPen(105, 50, 0);
    idTimerAll      =     	timer.add();
}
	
SNAKE_APP::~SNAKE_APP()
{
    this->release(blackBrushGame);
    this->release(blackBrushBorder);
    this->release(penBorder);
}
	
bool SNAKE_APP::init(const int numPlayer,const int idIcon)
{
	mbm::MONITOR_MANAGER manMonitor;
	mbm::MONITOR monitor;
	manMonitor.updateMonitors();
	if(!manMonitor.getMonitor(0, &monitor))
		return false;
	if(widthScreen ==0)
		widthScreen = monitor.width;
	if(heightScreen ==0)
		heightScreen = monitor.height - 50;
	players	=	numPlayer;
	this->timer.timeCntRunTime = 0;
	if(!win.init("Snake ++",widthScreen,heightScreen,0,0,0,false,true, false,nullptr,false,idIcon))
		return false;
    this->idGroupGame = win.addGroupBox("",10,10,widthScreen-20,heightScreen-140);
    win.setDrawer(this,this->idGroupGame);

    this->idGroupInfo = win.addGroupBox("",10,10 + heightScreen-120,widthScreen-20,100);
    win.setDrawer(&infoGame,this->idGroupInfo);
    RECT rect = win.getRect(this->idGroupGame);
    int margin = 15;
    int infoBottom = 70;
    for(unsigned int i=0; i< lsSnake.size(); i++)
	{
		SNAKE* snake = lsSnake[i];
		snake->restart(rect.right - rect.left,rect.bottom - rect.top,&timer,margin,infoBottom);
		tableGame	=	snake->bound;
        tableInfo.left = margin;
        tableInfo.right = rect.right - rect.left - margin;

        tableInfo.top = rect.bottom - infoBottom - margin + 15;
        tableInfo.bottom = tableInfo.top - margin + infoBottom;

		timer.set(snake->idTimerWalkSnake,0);
	}
	return true;
}
	
bool SNAKE_APP::render(COMPONENT_INFO &component)
{
    this->setPen(penBorder);
    this->setBrush(blackBrushBorder);
    this->drawRectangle(component.rect);

    this->setBrush(blackBrushGame);
    this->drawRectangle(tableGame.left,tableGame.top,tableGame.right - tableGame.left ,tableGame.bottom - tableGame.top);
	this->drawLine(maxScreen.left,maxScreen.bottom,maxScreen.right,maxScreen.bottom);

    this->setPen(penBorder);
    this->setBrush(blackBrushBorder);
    this->drawRectangle(tableInfo.left,tableInfo.top,tableInfo.right - tableInfo.left ,tableInfo.bottom - tableInfo.top);

	char playerMessage [1024];
    for(unsigned int i=0; i< lsSnake.size() && i < players; i++)
	{
		SNAKE* snake = lsSnake[i];
		snake->render(this);
        int x = tableInfo.left + 15;
        int y = tableInfo.top + (10 + (i* 25));
        if(snake->isGameOver)
        {
            sprintf(playerMessage,"Player [%d] %s",i+1,"GAME OVER!!!");
            this->drawText(x,y,255,100,0,playerMessage);
        }
        else
        {
            sprintf(playerMessage,"Player [%d] %s",i+1,snake->strBrowes);
		    this->drawText(x ,y,155, 50, 0,playerMessage);
        }
	}
return true;
}
	
void SNAKE_APP::update()
{
	if(GetKeyState(VK_LEFT) & 0x80)
		lsSnake[0]->set(DIR_TO_L);
	else if(GetKeyState(VK_RIGHT) & 0x80)
		lsSnake[0]->set(DIR_TO_R);
	else if(GetKeyState(VK_UP) & 0x80)
		lsSnake[0]->set(DIR_TO_U);
	else if(GetKeyState(VK_DOWN) & 0x80)
		lsSnake[0]->set(DIR_TO_D);

	if(players == 2)
	{
		if(GetKeyState('A') & 0x80)
			lsSnake[1]->set(DIR_TO_L);
		else if(GetKeyState('D') & 0x80)
			lsSnake[1]->set(DIR_TO_R);
		else if(GetKeyState('W') & 0x80)
			lsSnake[1]->set(DIR_TO_U);
		else if(GetKeyState('S') & 0x80)
			lsSnake[1]->set(DIR_TO_D);
	}

	if(GetKeyState(VK_ESCAPE) & 0x80)
		win.hide();
	for(unsigned int i=0; i< lsSnake.size() && i < players; i++)
	{
		SNAKE* snake = lsSnake[i];
		float t = timer.get(snake->idTimerWalkSnake);
		if(t >= snake->speed)
		{
			if(snake->move())
            {
			    win.refresh(this->idGroupGame);
            }
            timer.set(snake->idTimerWalkSnake,t - snake->speed);
		}
	}
    timer.updateFps();
	char str[255] = "";
    sprintf(str,"Time: %s",timer.getAsMinute());
    this->infoGame.infoTimer = str;
}
	
void SNAKE_APP::continueSnake()
{
	for(unsigned int i=0; i< lsSnake.size()&& i < players; i++)
	{
		SNAKE* snake = lsSnake[i];
		timer.set(snake->idTimerWalkSnake,0);
	}
	win.show();
}

INFO_GAME::INFO_GAME()
{
    blackBrushGame = this->createBrush(50,50,50);
    blackBrushBorder = this->createBrush(20,20,20);
    penBorder = this->createPen(105, 50, 0);
}

INFO_GAME::~INFO_GAME()
{
    this->release(blackBrushGame);
    this->release(blackBrushBorder);
    this->release(penBorder);
}

bool INFO_GAME::render(COMPONENT_INFO &component)
{
    this->setPen(penBorder);
    this->setBrush(blackBrushBorder);
    this->drawRectangle(component.rect);

    this->setBrush(blackBrushGame);
	this->drawRectangle(10,10,component.rect.right - 20,component.rect.bottom - 20);
	this->drawText(100,15,155, 50, 0,"Press ESC to pause");
	this->drawText(100,35,155, 50, 0,"Then menu trayIcon -> continue...");
	this->drawText(300,15,155, 50, 0,infoTimer.c_str());
    return true;
}

