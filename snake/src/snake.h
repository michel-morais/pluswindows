/*-----------------------------------------------------------------------------------------------------------------------|
| MIT License (MIT)                                                                                                      |
| by Adao and refactored by michel                                                                                       |
| Thanks Adao! I just refactored some functions however the code is pretty much the same!                                |
|                                                                                                                        |
| Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated           |
| documentation files (the "Software"), to deal in the Software without restriction, including without limitation        |
| the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and       |
| to permit persons to whom the Software is furnished to do so, subject to the following conditions:                     |
|                                                                                                                        |
| The above copyright notice and this permission notice shall be included in all copies or substantial portions of       |
| the Software.                                                                                                          |
|                                                                                                                        |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE   |
| WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR  |
| COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       |
| OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
|------------------------------------------------------------------------------------------------------------------------|
|  Snake game using plusWindows                                                                                          |
|-----------------------------------------------------------------------------------------------------------------------*/

#ifndef SNAKE_H
#define SNAKE_H


#include <plusWindows/plusWindows.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>

using namespace mbm;

class TIME_CONTROL
{	
	friend class		SNAKE;
public:
	TIME_CONTROL();
	unsigned int		fps;
	float				timeCntRunTime;
	float				fpsProp;
	unsigned int		timeCntCurrentTickCount;
	char* getAsMinute();
	unsigned int add(const float value=0);
	float get(const unsigned int index)const;
	bool set(const unsigned int index,const float newTime);
	void updateFps();
	
private:
	float				initialTime;
	unsigned int		lastTickCount;
	unsigned int		currentRate;
	unsigned int		countFps;
	unsigned int		rateToUpdateFps;
	std::vector<float>	aditionalTimes;
};

enum DIRECTION
{
	DIR_STOPED=0,
	DIR_TO_R=1,
	DIR_TO_L=2,
	DIR_TO_U=3,
	DIR_TO_D=4,
};

struct BROWSE//gomo
{
	DIRECTION	dir;
	int			x,y;
	BROWSE(DIRECTION direction,int posX,int posY);
	BROWSE();
};

class SNAKE
{
public:
	float				speed;
	float				incrementSpeed;
	float				minimumSpeed;
	float				initialSpeed;
	unsigned int 		idTimerWalkSnake;
	int					initialPosX,initialPosY;
	RECT				bound;
	char 				strBrowes[255];
	POINT*				food;
	bool				isGameOver;
	bool				eatedFood;
	SNAKE(	unsigned char r,
			unsigned char g, 
			unsigned char b,
			POINT* foodExtern,
			const int initialPositionX,
			const int initialPositionY,
            const int WPixel,
            mbm::DRAW* draw);
	virtual ~SNAKE();
	void release();
	void restart(int w,int h,TIME_CONTROL *timer,int margin,int infoBottom);
	const unsigned int getSize()const;
	void render(DRAW* draw);
	bool move();
	void addBrowse();
	void set(DIRECTION dir);
	
private:
	bool checkColision();
	bool collided(BROWSE* browse,BROWSE*	other);
	bool isCollededWithFood(BROWSE*	browse);
	bool eatFood();
	
    std::vector<BROWSE*>	lsBrowse;
	unsigned int			currentFirst;
	int						wPixel;
    int                     widthRect;
    int                     heightRect;
    HBRUSH          blackBrushSnake;
    HPEN            penBorderSnake;

    HBRUSH          brushBrowse;
    HPEN            penBrowse;
};

class INFO_GAME : public mbm::DRAW
{
	public:
    INFO_GAME();
    virtual ~INFO_GAME();
    bool render(COMPONENT_INFO &component);
    std::vector<std::string> infoSnake;
    std::string infoTimer;
private:
    HBRUSH          blackBrushGame;
    HBRUSH          blackBrushBorder;
    HPEN            penBorder;

};

class SNAKE_APP : public mbm::DRAW
{
	public:
	mbm::WINDOW 	win;
	TIME_CONTROL	timer;
	POINT			food;
	RECT			maxScreen;
    RECT			tableGame;
    RECT			tableInfo;
	
	unsigned int	players;
	std::vector<SNAKE*> lsSnake;
	SNAKE_APP(int sizeBlockSnake);
	virtual ~SNAKE_APP();
	bool init(const int numPlayer,const int idIcon=0);
    void continueSnake();
    void update();
    

private:
	bool render(COMPONENT_INFO &component);
    int				widthScreen;
	int				heightScreen;
	int             idGroupGame;
    int             idGroupInfo;
    HBRUSH          blackBrushGame;
    HBRUSH          blackBrushBorder;
    HPEN            penBorder;
    INFO_GAME       infoGame;
    unsigned int 	idTimerAll;
};


#endif

