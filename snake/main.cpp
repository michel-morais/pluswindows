/*-----------------------------------------------------------------------------------------------------------------------|
| MIT License (MIT)                                                                                                      |
| by Adao and refactored by michel                                                                                       |
| Thanks Adao! I just refactored some functions however the code is pretty much the same!                                |
|                                                                                                                        |
| Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated           |
| documentation files (the "Software"), to deal in the Software without restriction, including without limitation        |
| the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and       |
| to permit persons to whom the Software is furnished to do so, subject to the following conditions:                     |
|                                                                                                                        |
| The above copyright notice and this permission notice shall be included in all copies or substantial portions of       |
| the Software.                                                                                                          |
|                                                                                                                        |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE   |
| WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR  |
| COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       |
| OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
|------------------------------------------------------------------------------------------------------------------------|
|  Snake game using plusWindows                                                                                          |
|-----------------------------------------------------------------------------------------------------------------------*/


#ifdef __MINGW32__
	#define IDI_ICON1 0 
#elseif VS_2008_EXPRES
	#define IDI_ICON1 0 
	#define USE_THEME_WINDOWS	
#else
	#include "resource1.h"
	#define USE_THEME_WINDOWS	
#endif

#include "resource1.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include "src/snake.h"
#include "../src/plusWindows/defaultThemePlusWindows.h"

using namespace mbm;

struct ID
{
	int idTrayIconExit;
	int idTrayIconSnakeStart1;
	int idTrayIconSnakeStart2;
	int idTrayIconSnakeContinue;
};


void onMenuTrayIcon(WINDOW* w, DATA_EVENT &dataEvent)
{
	ID* ids = static_cast<ID*>(w->getObjectContext(0));
	if(ids)
	{
        const int id = dataEvent.idComponent;
		SNAKE_APP* snakeApp = static_cast<SNAKE_APP*>(w->getObjectContext(1));
		if(snakeApp)
		{	
			if(id == ids->idTrayIconSnakeStart1)
				snakeApp->init(1);
			else if(id == ids->idTrayIconSnakeStart2)
				snakeApp->init(2);
			else if(id == ids->idTrayIconSnakeContinue)
				snakeApp->continueSnake();
			else if(id == ids->idTrayIconExit)
				w->run = false;
		}
	}
}


void loop(WINDOW* w, DATA_EVENT &dataEvent)
{
	SNAKE_APP* snakeApp = static_cast<SNAKE_APP*>(w->getObjectContext(1));
	if(snakeApp && snakeApp->win.isVisible)
	{
		snakeApp->update();
	}
}

/*
Old code using 'plusWindows.h' as render.
Please note that this is an old and the code is ugly because Adao was learning C++ and it was done like this!
the times were other!

*/
int main(int argc,char** argv)
{
    int sizeBlockSnake = 16;
	SNAKE_APP	snakeApp(sizeBlockSnake);
	ID			id;
    THEME_WINPLUS_CUSTOM_RENDER::setTheme(22,false);
	snakeApp.win.setObjectContext(&id,0);
	snakeApp.win.setObjectContext(&snakeApp,1);
	if(snakeApp.init(1,IDI_ICON1))
	{
		snakeApp.win.hideConsoleWindow();
									   snakeApp.win.addTrayIcon(IDI_ICON1,onMenuTrayIcon,"Snake++");
		id.idTrayIconSnakeStart1	=  snakeApp.win.addMenuTrayIcon("Start 1 player!",-1,0,0,true);
		id.idTrayIconSnakeStart2	=  snakeApp.win.addMenuTrayIcon("Start 2 player!",-1);
		id.idTrayIconSnakeContinue	=  snakeApp.win.addMenuTrayIcon("Continue...",-1);
		id.idTrayIconExit			=  snakeApp.win.addMenuTrayIcon("Quit",-1);
		snakeApp.win.exitOnEsc		=  false;
		snakeApp.win.hideOnExit		=	true;
		return snakeApp.win.enterLoop(loop);
	}
	return 1;
}

