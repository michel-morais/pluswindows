
#include "src/convolucao.h"
#include "../src/plusWindows/defaultThemePlusWindows.h"

/*
By IRAJA
Thanks Iraja for do this

*/

int main(int argv,char** argc)
{
    int VIEWx = 600;
    int VIEWy = 400;
    mbm::WINDOW w;
    DRAW_GRAF graf(VIEWx,VIEWy);

    THEME_WINPLUS_CUSTOM_RENDER::setTheme(13,true);
    
    w.init("Convolucao",VIEWx + 20, VIEWy + 80,0,0,false,false,true,false,onChangeWindow,false,0);

    
    
    graf.minWidth = VIEWx + 20;
	graf.minHeight= VIEWy + 80;
    
    graf.idGroup = w.addGroupBox("", 10, 70, VIEWx, VIEWy);
    
    w.setDrawer(&graf, graf.idGroup);
    

    w.hideConsoleWindow();
    w.addLabel("Sinal de Entrada", 10, 10, 110, 20, -1);
    w.addLabel("Sinal do Sistema", 10, 40, 110, 20, -1);
    graf.idTextBox1 = w.addTextBox("-6,0:-5,0:-4,0:-3,0:-2,0:-1,0:0,0:1,0:2,0:3,0:4,0:5,0:6,0", 125, 10, VIEWx - 300, 20, -1);
    graf.idTextBox2 = w.addTextBox("-6,0:-5,0:-4,0:-3,0:-2,0:-1,0:0,0:1,0:2,0:3,0:4,0:5,0:6,0", 125, 40, VIEWx - 300, 20, -1);
    graf.idComboBox1 = w.addCombobox(VIEWx - 170, 10, 100, 110, onPressCombobox1);
    graf.idComboBox2 = w.addCombobox(VIEWx - 170, 40, 100, 105, onPressCombobox2);

	w.addText(graf.idComboBox1, "Par");
	w.addText(graf.idComboBox1, "Impar");
	w.addText(graf.idComboBox1, "Unitario");
	w.addText(graf.idComboBox1, "Degrau");
	w.addText(graf.idComboBox1, "Sinal");
	w.addText(graf.idComboBox1, "Retangular");
	w.addText(graf.idComboBox1, "Seno");
	w.addText(graf.idComboBox1, "Cosseno");
		
	w.addText(graf.idComboBox2, "Par");
	w.addText(graf.idComboBox2, "Impar");
	w.addText(graf.idComboBox2, "Unitario");
	w.addText(graf.idComboBox2, "Degrau");
	w.addText(graf.idComboBox2, "Sinal");
	w.addText(graf.idComboBox2, "Retangular");
	w.addText(graf.idComboBox2, "Seno");
	w.addText(graf.idComboBox2, "Cosseno");

    w.addButton("OK", VIEWx - 65, 10, 25, 20, -1, onPressOkEntrada);
    w.addButton("OK", VIEWx - 65, 40, 25, 20, -1, onPressOkSistema);

    w.addSpinInt(VIEWx - 35, 10, 50, 20, -1, 0, 0, onPressSpin1, 2, 30, 5, 20);
    w.addSpinInt(VIEWx - 35, 40, 50, 20, -1, 0, 0, onPressSpin2, 2, 30, 5, 20);
    w.setObjectContext(&graf,0);
    w.enterLoop(NULL);


    return(0);
}