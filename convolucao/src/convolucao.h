/*-----------------------------------------------------------------------------------------------------------------------|
|  Example using pluswindows to render some charts (convolucao)                                                          |
|-----------------------------------------------------------------------------------------------------------------------*/

/*
By IRAJA
*/

#ifndef CONVOLUCAO_WINDOWS_H
#define CONVOLUCAO_WINDOWS_H


#include <plusWindows/plusWindows.h>

using namespace mbm;

class DRAW_GRAF : public mbm::DRAW
{
    public:
		int idGroup;
		int minWidth;
		int minHeight;
        int VIEWx;
        int VIEWy;
        typedef struct view
        {
            int inicioX;
            int inicioY;
            int fimX;
            int fimY;
            int escala;
        }View;

        View view[3];
        int sinalEntrada[2][100], sinalSistema[2][100], sinalSaida[2][100];
        int idTextBox1, idTextBox2, idComboBox1, idComboBox2;
        int amostrasEnt, amostrasSis, amostrasSaida, margem;

        DRAW_GRAF(const int _VIEWx,const int _VIEWy);
        virtual ~DRAW_GRAF();
private:
        bool render(COMPONENT_INFO &component);
        virtual int eraseBackGround(COMPONENT_INFO &component);
        HBRUSH whiteBrush,greenBrush,blueBrush,redBrush;
        HPEN penBlack,penGray,penRed,penBlue,penGreen;
};


/*
NPP_SAVE
g++ "C:\Users\t178242\pocketcpp\IRAJA\main.cpp" -g
-o "C:\Users\t178242\pocketcpp\IRAJA\graf" -static -std=c++0x 
"C:\Users\t178242\pocketcpp\MinGW\lib\libws2_32.a" 
"C:\Users\t178242\pocketcpp\MinGW\lib\libgdi32.a"  
"C:\Users\t178242\pocketcpp\MinGW\lib\libcomctl32.a" 
"C:\Users\t178242\pocketcpp\MinGW\lib\libcomdlg32.a" -Wall 
*/
void onPressOkEntrada(WINDOW* w, DATA_EVENT &dataEvent);

void onPressOkSistema(WINDOW* w, DATA_EVENT &dataEvent);

void onPressSpin1(WINDOW* w, DATA_EVENT &dataEvent);

void onPressSpin2(WINDOW* w, DATA_EVENT &dataEvent);

void onPressCombobox1(WINDOW* w, DATA_EVENT &dataEvent);

void onPressCombobox2(WINDOW* w, DATA_EVENT &dataEvent);

void onChangeWindow(WINDOW* w, DATA_EVENT &dataEvent);

#endif