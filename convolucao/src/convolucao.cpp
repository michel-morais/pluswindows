/*-----------------------------------------------------------------------------------------------------------------------|
|  Example using pluswindows to render some charts (convolucao)                                                          |
|-----------------------------------------------------------------------------------------------------------------------*/

/*
By IRAJA
*/


#include "convolucao.h"


DRAW_GRAF::DRAW_GRAF(const int _VIEWx,const int _VIEWy):VIEWx(_VIEWx),VIEWy(_VIEWy)
{
    memset(sinalEntrada, 0, sizeof(sinalEntrada));
    memset(sinalSistema, 0, sizeof(sinalSistema));
    memset(sinalSaida  , 0, sizeof(sinalSaida));

    idTextBox1    = 0;
    idTextBox2    = 0;
    amostrasEnt   = 0;
    amostrasSis   = 0;
    amostrasSaida = 0;
    margem        = 10;

    minWidth = minHeight = 0;
    idComboBox1 = idComboBox2 = idGroup = -1;

    //Ponto Finfal == Ponto Inicial + N*Pixels
    view[0].inicioX = margem;
    view[0].inicioY = margem;
    view[0].fimX    = (VIEWx / 2) - margem - (margem / 2);
    view[0].fimY    = (VIEWy / 2) - margem - (margem / 2);
    view[0].escala  = 20;
            
	view[1].inicioX = margem;
	view[1].inicioY = (VIEWy / 2) + (margem / 2);
    view[1].fimX    = (VIEWx / 2) - (margem / 2) - margem;
    view[1].fimY    = (VIEWy / 2) - (margem / 2) - margem;
    view[1].escala  = 20;
            
	view[2].inicioX = (VIEWx / 2) + (margem / 2);
    view[2].inicioY = margem;
    view[2].fimX    = (VIEWx / 2) - (margem / 2) - margem;
    view[2].fimY    =  VIEWy      -  margem      - margem;
    view[2].escala  = 20;

    whiteBrush = this->createBrush(255,255,255);
    greenBrush = this->createBrush(0,150,0);
    blueBrush = this->createBrush(0,0,250);
    redBrush = this->createBrush(255,0,0);
    penBlack = this->createPen(0,0,0);
    penGray =  this->createPen(200,200,200);
    penRed =  this->createPen(255,0,0);
    penBlue = this->createPen(0,0,255);
    penGreen = this->createPen(0,150,0);
}

DRAW_GRAF::~DRAW_GRAF()
{
    this->release(whiteBrush);
    this->release(greenBrush);
    this->release(blueBrush);
    this->release(redBrush);
    this->release(penBlack);
    this->release(penGray);
    this->release(penRed);
    this->release(penBlue);
    this->release(penGreen);
}

int DRAW_GRAF::eraseBackGround(COMPONENT_INFO &component)
{
    this->setBrush(whiteBrush);
    this->drawRectangle(component.rect);
    return 1;
}
        
bool DRAW_GRAF::render(COMPONENT_INFO &component)
{
    this->setBrush(whiteBrush);
	//======================== Cria as Views ==========================
    this->setPen(penBlack);
    //View1
    this->drawRectangle(view[0].inicioX, view[0].inicioY,
                        view[0].fimX   , view[0].fimY   );

    //View2
    this->drawRectangle(view[1].inicioX, view[1].inicioY,
                        view[1].fimX   , view[1].fimY   );

    //View3
    this->drawRectangle(view[2].inicioX, view[2].inicioY,
                        view[2].fimX   , view[2].fimY   );

    //======================== Cria os Eixos ==========================
    this->setPen(penGray);
    //Eixo x da View1
    this->drawLine(view[0].inicioX + margem, (view[0].fimY / 2) + margem,
                    view[0].fimX            , (view[0].fimY / 2) + margem);
                
    //Eixo y da View1
    this->drawLine((view[0].fimX / 2) + margem, view[0].inicioY + margem,
                    (view[0].fimX / 2) + margem, view[0].fimY            );
                
    //Eixo x  da View2
    this->drawLine(view[1].inicioX + margem, VIEWy - (view[1].fimY / 2) - margem,
                    view[1].fimX            , VIEWy - (view[1].fimY / 2) - margem);
        
    //Eixo y da View2
    this->drawLine((view[1].fimX / 2) + margem, view[1].inicioY + margem,
                    (view[1].fimX / 2) + margem, VIEWy - margem  - margem);
                
    //Eixo x  da View3
    this->drawLine(view[2].inicioX + margem, VIEWy / 2,
                    VIEWx - margem - margem , VIEWy / 2);
        
    //Eixo y da View3
	this->drawLine(VIEWx - (view[2].fimX / 2) - margem, view[2].inicioY + margem,
                    VIEWx - (view[2].fimX / 2) - margem, view[2].fimY            );

	//======================== Cria as Escalas ==========================
    //Escala x da View1
    this->setPen(penBlack);
    for(int index = (view[0].fimX / 2) + margem; index >= view[0].inicioX + margem; index-=view[0].escala)
        this->drawLine(index, (view[0].fimY / 2) + margem - 3,
                        index, (view[0].fimY / 2) + margem + 3);
    for(int index = (view[0].fimX / 2) + margem; index <= view[0].fimX; index+=view[0].escala)
        this->drawLine(index, (view[0].fimY / 2) + margem - 3,
                        index, (view[0].fimY / 2) + margem + 3);

    //Escala y da View1
    for(int index = (view[0].fimY / 2) + margem; index >= view[0].inicioY + margem; index-=view[0].escala)
        this->drawLine((view[0].fimX / 2) + margem - 3,  index,
                        (view[0].fimX / 2) + margem + 3,  index);
    for(int index = (view[0].fimY / 2) + margem; index <= view[0].fimY; index+=view[0].escala)
        this->drawLine((view[0].fimX / 2) + margem - 3,  index,
                        (view[0].fimX / 2) + margem + 3,  index);

    //Escala x da View2
    for(int index = (view[1].fimX / 2) + margem; index >= view[1].inicioX + margem; index-=view[1].escala)
        this->drawLine(index, VIEWy - (view[1].fimY / 2) - margem - 3,
                        index, VIEWy - (view[1].fimY / 2) - margem + 3);
    for(int index = (view[1].fimX / 2) + margem; index <= view[1].fimX; index+=view[1].escala)
        this->drawLine(index, VIEWy - (view[1].fimY / 2) - margem - 3,
                        index, VIEWy - (view[1].fimY / 2) - margem + 3);

    //Escala y da View2                
    for(int index = view[1].inicioY + (view[1].inicioY / 2) - margem; index >= view[1].inicioY + margem; index-=view[1].escala)
        this->drawLine((view[1].fimX / 2) + margem - 3,  index,
                        (view[1].fimX / 2) + margem + 3,  index);
    for(int index = view[1].inicioY + (view[1].inicioY / 2) - margem; index <= VIEWy - margem - margem; index+=view[1].escala)
        this->drawLine((view[1].fimX / 2) + margem - 3,  index,
                        (view[1].fimX / 2) + margem + 3,  index);

    //Escala x da View3
    for(int index = VIEWx - (view[2].fimX / 2) - margem; index >= view[2].inicioX + margem; index-=view[2].escala)
        this->drawLine(index, (view[2].fimY / 2) + margem - 3,
                        index, (view[2].fimY / 2) + margem + 3);
    for(int index = VIEWx - (view[2].fimX / 2) - margem; index <= VIEWx - margem - margem; index+=view[2].escala)
        this->drawLine(index, (view[2].fimY / 2) + margem - 3,
                        index, (view[2].fimY / 2) + margem + 3);

    //Escala y da View3                
        for(int index = (view[2].fimY / 2) + margem; index >= view[2].inicioY + margem; index-=view[2].escala)
            this->drawLine(VIEWx - (view[2].fimX / 2) - margem - 3,  index,
                            VIEWx - (view[2].fimX / 2) - margem + 3,  index);
        for(int index = (view[2].fimY / 2) + margem; index <= VIEWy - margem - margem; index+=view[2].escala)
            this->drawLine(VIEWx - (view[2].fimX / 2) - margem - 3,  index,
                            VIEWx - (view[2].fimX / 2) - margem + 3,  index);

	//======================== Plota os Sinais ==========================
    this->setPen(penBlue);
    //Plota Sinal de Entrada
    for(int index = 0; index < amostrasEnt; index++)
    {
        this->drawLine((view[0].fimX / 2) + ( sinalEntrada[0][index] * view[0].escala) + margem, (view[0].fimY / 2) + margem,
                        (view[0].fimX / 2) + ( sinalEntrada[0][index] * view[0].escala) + margem,
                        (view[0].fimY / 2) + (-sinalEntrada[1][index] * view[0].escala) + margem);
        //this->drawCircleFilled
        this->setBrush(blueBrush);
        this->drawCircle((view[0].fimX / 2) + ( sinalEntrada[0][index] * view[0].escala) + margem,
                                (view[0].fimY / 2) + (-sinalEntrada[1][index] * view[0].escala) + margem,  3);
    }
    this->setPen(penRed);
    //Plota Sinal do Sistema
    for(int index = 0; index < amostrasSis; index++)
    {
        this->drawLine((view[1].fimX / 2) + ( sinalSistema[0][index] * view[1].escala) + margem, VIEWy - (view[1].fimY / 2) - margem,
                        (view[1].fimX / 2) + ( sinalSistema[0][index] * view[1].escala) + margem, VIEWy - (view[1].fimY / 2)
							                + (-sinalSistema[1][index] * view[1].escala) - margem);
        //this->drawCircleFilled
        this->setBrush(redBrush);
        this->drawCircle((view[1].fimX / 2) + ( sinalSistema[0][index] * view[1].escala) + margem,
                            VIEWy - (view[1].fimY / 2) + (-sinalSistema[1][index] * view[1].escala) - margem,  3);
    }


    //Plota Sinal de Saida
    amostrasSaida = amostrasEnt + amostrasSis - 1;
    memset(sinalSaida, 0, sizeof(sinalSaida));
    sinalSaida[0][0] = sinalEntrada[0][0] + sinalSistema[0][0];
    this->setPen(penGreen);
    for(int coluna = 0, colunaE = 0, colunaS = 0; coluna < amostrasSaida; coluna++, colunaE = 0, colunaS = coluna)
    {
        sinalSaida[0][coluna + 1] = sinalSaida[0][coluna] + 1;
        
            //while(colunaS >= 0)
            //while(colunaE <= coluna && colunaS >= 0)
            while(colunaE <= coluna)
                sinalSaida[1][coluna] += sinalEntrada[1][colunaE++] * sinalSistema[1][colunaS--];
            /*
            sinalSaida[1][0] = sinalEntrada[1][0] * sinalSistema[1][0];
            sinalSaida[1][1] = sinalEntrada[1][0] * sinalSistema[1][1] + sinalEntrada[1][1] * sinalSistema[1][0];
            sinalSaida[1][2] = sinalEntrada[1][0] * sinalSistema[1][2] + sinalEntrada[1][1] * sinalSistema[1][1] + sinalEntrada[1][2] * sinalSistema[1][0];
            sinalSaida[1][3] = sinalEntrada[1][0] * sinalSistema[1][3] + sinalEntrada[1][1] * sinalSistema[1][2] + sinalEntrada[1][2] * sinalSistema[1][1] + sinalEntrada[1][3] * sinalSistema[1][0];
            sinalSaida[1][4] = sinalEntrada[1][0] * sinalSistema[1][4] + sinalEntrada[1][1] * sinalSistema[1][3] + sinalEntrada[1][2] * sinalSistema[1][2] + sinalEntrada[1][3] * sinalSistema[1][1] + sinalEntrada[1][4] * sinalSistema[1][0];
            */
            this->setBrush(whiteBrush);
            this->drawLine(VIEWx - (view[2].fimX / 2) + ( sinalSaida[0][coluna] * view[2].escala) - margem, (view[2].fimY / 2) + margem,
                            VIEWx - (view[2].fimX / 2) + ( sinalSaida[0][coluna] * view[2].escala) - margem,
                                    view[2].fimY / 2  + (-sinalSaida[1][coluna] * view[2].escala) + margem);
            //this->drawCircleFilled
            this->setBrush(greenBrush);
            this->drawCircle(VIEWx - (view[2].fimX / 2) + ( sinalSaida[0][coluna] * view[2].escala) - margem,
                                            view[2].fimY / 2  + (-sinalSaida[1][coluna] * view[2].escala) + margem, 3);
    }
    return true;
}



/*
NPP_SAVE
g++ "C:\Users\t178242\pocketcpp\IRAJA\main.cpp" -g
-o "C:\Users\t178242\pocketcpp\IRAJA\graf" -static -std=c++0x 
"C:\Users\t178242\pocketcpp\MinGW\lib\libws2_32.a" 
"C:\Users\t178242\pocketcpp\MinGW\lib\libgdi32.a"  
"C:\Users\t178242\pocketcpp\MinGW\lib\libcomctl32.a" 
"C:\Users\t178242\pocketcpp\MinGW\lib\libcomdlg32.a" -Wall 
*/
void onPressOkEntrada(WINDOW* w, DATA_EVENT &dataEvent)
{
    char texto[255];
    std::vector<std::string> results1;
    std::vector<std::string> results2;
    DRAW_GRAF *graf = (DRAW_GRAF *) w->getObjectContext(0);
    memset(graf->sinalEntrada, 0, sizeof(graf->sinalEntrada));
        
    //Montando sinal de entrada
    w->getText(graf->idTextBox1, texto, sizeof(texto));
    split(results1,texto,':');
    graf->amostrasEnt = results1.size();
        
    for(unsigned int coluna = 0; coluna < results1.size(); coluna++)
    {
        split(results2,results1[coluna].c_str(), ',');


            if(results2.size() == 2)
            {
                graf->sinalEntrada[0][coluna] = atoi(results2[0].c_str());
                graf->sinalEntrada[1][coluna] = atoi(results2[1].c_str());
            }
            results2.clear();
    }
    w->refresh(graf->idGroup,true);
}
void onPressOkSistema(WINDOW* w, DATA_EVENT &dataEvent)
{
    char texto[255];
        std::vector<std::string> results1;
        std::vector<std::string> results2;
        DRAW_GRAF *graf = (DRAW_GRAF *) w->getObjectContext(0);
        memset(graf->sinalSistema, 0, sizeof(graf->sinalSistema));
        
        //Montando sinal do Sistema
		w->getText(graf->idTextBox2, texto, sizeof(texto));
        split(results1,texto, ':');
        graf->amostrasSis = results1.size();


        for(unsigned int coluna = 0; coluna < results1.size(); coluna++)
        {
            split(results2,results1[coluna].c_str(), ',');


                if(results2.size() == 2)
                {
                    graf->sinalSistema[0][coluna] = atoi(results2[0].c_str());
                    graf->sinalSistema[1][coluna] = atoi(results2[1].c_str());
                }
                results2.clear();
        }
        w->refresh(graf->idGroup,true);
}


void onPressSpin1(WINDOW* w, DATA_EVENT &dataEvent)
{
    DRAW_GRAF *graf = (DRAW_GRAF *) w->getObjectContext(0);
    graf->view[0].escala = dataEvent.getAsInt();
    graf->view[1].escala = dataEvent.getAsInt();
    w->refresh(graf->idGroup,true);
}


void onPressSpin2(WINDOW* w, DATA_EVENT &dataEvent)
{
    DRAW_GRAF *graf = (DRAW_GRAF *) w->getObjectContext(0);
    graf->view[2].escala = dataEvent.getAsInt();
    w->refresh(graf->idGroup,true);
}


void onPressCombobox1(WINDOW* w, DATA_EVENT &dataEvent)
{
    int valor = 0;
	DRAW_GRAF *graf = (DRAW_GRAF *) w->getObjectContext(0);

	valor = w->getSelectedIndex(graf->idComboBox1);

	if(valor == 0)//Simetria Par
	    w->setText(graf->idTextBox1, "-3,-2:-2,-2:-1,2:0,2:1,2:2,-2:3,-2");
	
    if(valor == 1)//Simetria Impar
        w->setText(graf->idTextBox1, "-3,-2:-2,-2:-1,-2:0,0:1,2:2,2:3,2");

    if(valor == 2)//Impulso Unitario
	    w->setText(graf->idTextBox1, "-3,0:-2,0:-1,0:0,2:1,0:2,0:3,0");

    if(valor == 3)//Funcao Degrau
        w->setText(graf->idTextBox1, "-3,0:-2,0:-1,0:0,1:1,1:2,1:3,1");

	if(valor == 4)//Funcao Sinal
        w->setText(graf->idTextBox1, "-3,-1:-2,-1:-1,-1:0,1:1,1:2,1:3,1");
		
	if(valor == 5)//Retangular
        w->setText(graf->idTextBox1, "-4,0:-3,0:-2,1:-1,1:0,1:1,1:2,1:3,0:4,0");
	
	if(valor == 6)//Seno
        w->setText(graf->idTextBox1, "-6,2:-5,1:-4,-0:-3,-1:-2,-2:-1,-1:0,0:1,1:2,2:3,1:4,0:5,-1:6,-2");
	
	if(valor == 7)//Cosseno
        w->setText(graf->idTextBox1, "-6,-0:-5,-1:-4,-2:-3,-1:-2,0:-1,1:0,2:1,1:2,0:3,-1:4,-2:5,-1:6,0");
    w->refresh(graf->idGroup,true);
}


void onPressCombobox2(WINDOW* w, DATA_EVENT &dataEvent)
{
    int valor = 0;
	DRAW_GRAF *graf = (DRAW_GRAF *) w->getObjectContext(0);

	valor = w->getSelectedIndex(graf->idComboBox2);

	if(valor == 0)//Simetria Par
	    w->setText(graf->idTextBox2, "-3,-2:-2,-2:-1,2:0,2:1,2:2,-2:3,-2");
	
    if(valor == 1)//Simetria Impar
        w->setText(graf->idTextBox2, "-3,-2:-2,-2:-1,-2:0,0:1,2:2,2:3,2");

    if(valor == 2)//Impulso Unitario
	    w->setText(graf->idTextBox2, "-3,0:-2,0:-1,0:0,2:1,0:2,0:3,0");

    if(valor == 3)//Funcao Degrau
        w->setText(graf->idTextBox2, "-3,0:-2,0:-1,0:0,1:1,1:2,1:3,1");

	if(valor == 4)//Funcao Sinal
        w->setText(graf->idTextBox2, "-3,-1:-2,-1:-1,-1:0,1:1,1:2,1:3,1");
		
	if(valor == 5)//Retangular
        w->setText(graf->idTextBox2, "-4,0:-3,0:-2,1:-1,1:0,1:1,1:2,1:3,0:4,0");
	
	if(valor == 6)//Seno
        w->setText(graf->idTextBox2, "-6,2:-5,1:-4,-0:-3,-1:-2,-2:-1,-1:0,0:1,1:2,2:3,1:4,0:5,-1:6,-2");
	
	if(valor == 7)//Cosseno
        w->setText(graf->idTextBox2, "-6,-0:-5,-1:-4,-2:-3,-1:-2,0:-1,1:0,2:1,1:2,0:3,-1:4,-2:5,-1:6,0");
    w->refresh(graf->idGroup,true);
}


void onChangeWindow(WINDOW* w, DATA_EVENT &dataEvent)
{
    const int message = dataEvent.getAsInt();
	if(message == WM_SIZE)
	{
		DRAW_GRAF *graf = (DRAW_GRAF *) w->getObjectContext(0);
        if(graf)
        {
		    graf->VIEWx = w->getWidth();
		    graf->VIEWy = w->getHeight();
		    if(graf->VIEWx < graf->minWidth || graf->VIEWy < graf->minHeight)
		    {
			    graf->VIEWx = graf->minWidth;
			    graf->VIEWy = graf->minHeight;
		    }
		    w->resize(graf->idGroup,graf->VIEWx - 20,graf->VIEWy - 80);
        }
	}
	else if(message == WM_CLOSE || message == WM_QUIT || message == WM_QUERYENDSESSION)
	{
		w->run = false;
	}
}

