/*-----------------------------------------------------------------------------------------------------------------------|
| MIT License (MIT)                                                                                                      |
| Copyright (C) 2017 by Michel Braz de Morais  <michel.braz.morais@gmail.com>                                            |
|                                                                                                                        |
| Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated           |
| documentation files (the "Software"), to deal in the Software without restriction, including without limitation        |
| the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and       |
| to permit persons to whom the Software is furnished to do so, subject to the following conditions:                     |
|                                                                                                                        |
| The above copyright notice and this permission notice shall be included in all copies or substantial portions of       |
| the Software.                                                                                                          |
|                                                                                                                        |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE   |
| WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR  |
| COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       |
| OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
|------------------------------------------------------------------------------------------------------------------------|
|  Example of use the lib plusWindows for owner-drawn of buttons, labels, radio, checkbox ... etc                        |
|-----------------------------------------------------------------------------------------------------------------------*/



#ifndef BRAZIL_FLAG_WINDOWS_H
#define BRAZIL_FLAG_WINDOWS_H

#include <plusWindows/plusWindows.h>

using namespace mbm;

class BRAZIL_FLAG : public mbm::DRAW
{
public:
	
	int w;
	int h;
	int w_2,w_10;
	int h_2,h_10;
	int ray;
	RECT rectGreen,rectBallOrder;
	POINT poly[4];
	POINT pos[3],star[10];
	POINT ballOrder,orderprogress;
	bool incrementX[3];
	bool incrementY[3];
    const int idgroup;

	mbm::BMP bmp;

	BRAZIL_FLAG(mbm::WINDOW* parent,const int idgroup);
	
	void moveFigures();
	bool render(mbm::COMPONENT_INFO &component);
private:
    static void update(mbm::WINDOW* w,mbm::DATA_EVENT &dataEvent);

};

#endif