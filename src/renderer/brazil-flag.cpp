/*-----------------------------------------------------------------------------------------------------------------------|
| MIT License (MIT)                                                                                                      |
| Copyright (C) 2017 by Michel Braz de Morais  <michel.braz.morais@gmail.com>                                            |
|                                                                                                                        |
| Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated           |
| documentation files (the "Software"), to deal in the Software without restriction, including without limitation        |
| the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and       |
| to permit persons to whom the Software is furnished to do so, subject to the following conditions:                     |
|                                                                                                                        |
| The above copyright notice and this permission notice shall be included in all copies or substantial portions of       |
| the Software.                                                                                                          |
|                                                                                                                        |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE   |
| WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR  |
| COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       |
| OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
|------------------------------------------------------------------------------------------------------------------------|
|  Example of use the lib plusWindows for owner-drawn of buttons, labels, radio, checkbox ... etc                        |
|-----------------------------------------------------------------------------------------------------------------------*/

#include "brazil-flag.h"



BRAZIL_FLAG::BRAZIL_FLAG(WINDOW* parent,const int _idgroup):idgroup(_idgroup)
{
	memset(pos,0,sizeof(pos));
	this->w	= 275;
	this->h	= 150;
	this->w_2 = w / 2;
	this->h_2 = h / 2;
	this->w_10 = w / 10;
	this->h_10 = h / 10;
	this->ray = w_10 * 2 - (w_10 / 2);

	this->ballOrder.x = w_2;
	this->ballOrder.y = h_2;
    parent->addTimer(static_cast<int>(1000.0f / 60.0f), BRAZIL_FLAG::update,this);//60 fps
}
	
void BRAZIL_FLAG::update(WINDOW* w, DATA_EVENT &dataEvent)
{
    BRAZIL_FLAG* that = static_cast<BRAZIL_FLAG*>(dataEvent.userDrawer);
    w->refresh(that->idgroup,false);//force re draw 60 fps
}

void BRAZIL_FLAG::moveFigures()
{
	for(int i=0; i< 3; i++)
	{
		if(incrementX[i])
			pos[i].x++;
		else
			pos[i].x--;

		if(incrementY[i])
			pos[i].y++;
		else
			pos[i].y--;

		if(pos[i].x > ( 10 + i))
			incrementX[i] = false;
		if(pos[i].x < ( -10 - i))
			incrementX[i] = true;

		if(pos[i].y > ( 8 + i))
			incrementY[i] = false;
		if(pos[i].y < ( -8 - i))
			incrementY[i] = true;
	}
		
	//Coordenadas do losango (parte amarela )
	poly[0].x = w_10 + pos[1].x+ getRandomInt(-1,1);
	poly[0].y = h_2 + pos[1].y+ getRandomInt(-1,1);

	poly[1].x = w_2 + pos[1].x+ getRandomInt(-1,1);
	poly[1].y = h_10 + pos[1].y+ getRandomInt(-1,1);

	poly[2].x = w - w_10 + pos[1].x+ getRandomInt(-1,1);
	poly[2].y = h_2 + pos[1].y+ getRandomInt(-1,1);

	poly[3].x = w_2 + pos[1].x+ getRandomInt(-1,1);
	poly[3].y = h - h_10 + pos[1].y+ getRandomInt(-1,1);

	//Coordenadas da bandeira (parte verde maior)
	rectGreen.left = pos[2].x;
	rectGreen.top  = pos[2].y;
	rectGreen.right = pos[2].x + w - rectGreen.left;
	rectGreen.bottom  = pos[2].y + h - rectGreen.top;

	orderprogress.x = w_2 - w_10 + pos[0].x;
	orderprogress.y = w_2 - h_2 + pos[0].y;

	//Coordenadas da estrela obtidas no paint (dae s� basta escalar)
	star[0].x = 0;
	star[0].y = 27;

	star[1].x = 29;
	star[1].y = 27;

	star[2].x = 39;
	star[2].y = 0;

	star[3].x = 48;
	star[3].y = 27;

	star[4].x = 76;
	star[4].y = 27;

	star[5].x = 53;
	star[5].y = 42;

	star[6].x = 62;
	star[6].y = 69;

	star[7].x = 39;
	star[7].y = 52;
		
	star[8].x = 15;
	star[8].y = 69;

	star[9].x = 24;
	star[9].y = 43;

	for(unsigned int i=0; i< 10; i++)//escala 0.2,  move e mexe os bra�os da estrela randomicamente
	{
		star[i].x = (long)((float)star[i].x * 0.2f) + orderprogress.x + w_10 + getRandomInt(-1,1);
		star[i].y = (long)((float)star[i].y * 0.2f) + orderprogress.y + (h_10 * 2)  + getRandomInt(-1,1);
	}
	//Coordenadas da esfera azul desenhada como elipse
	rectBallOrder.left = ballOrder.x + pos[0].x - ray + getRandomInt(-2,2);
	rectBallOrder.right = ballOrder.x + pos[0].x + ray + getRandomInt(-2,2);

	rectBallOrder.top = ballOrder.y + pos[0].y - ray ;
	rectBallOrder.bottom = ballOrder.y + pos[0].y + ray;
}

bool BRAZIL_FLAG::render(COMPONENT_INFO &)
{
    this->moveFigures();
	this->selectBrushColor(0,149,0);
    this->drawRectangle(rectGreen);
	this->selectBrushColor(255,255,0);
    this->drawPoygon(poly,4);
	this->selectBrushColor(0,0,255);
    this->drawElipse(rectBallOrder);
	this->drawText(orderprogress.x,  orderprogress.y,255,255,255,0,0,255, "Ordem &");
	this->drawText(orderprogress.x,orderprogress.y + h_10,255,255,255,0,0,255, "Progresso");
	this->selectBrushColor(255,255,255);
    this->drawPoygon(star,10);
    return true;
}
