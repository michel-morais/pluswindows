/*-----------------------------------------------------------------------------------------------------------------------|
| MIT License (MIT)                                                                                                      |
| Copyright (C) 2015      by Michel Braz de Morais  <michel.braz.morais@gmail.com>                                       |
|                                                                                                                        |
| Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated           |
| documentation files (the "Software"), to deal in the Software without restriction, including without limitation        |
| the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and       |
| to permit persons to whom the Software is furnished to do so, subject to the following conditions:                     |
|                                                                                                                        |
| The above copyright notice and this permission notice shall be included in all copies or substantial portions of       |
| the Software.                                                                                                          |
|                                                                                                                        |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE   |
| WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR  |
| COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       |
| OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
|                                                                                                                        |
|-----------------------------------------------------------------------------------------------------------------------*/

#include "redirectFunction.h"

    REDIRECT_FUNCTION::REDIRECT_FUNCTION(const char *lpFileNameDll, const char *lpNameFunctionToRedirect, LPVOID newFunctionAdress)
    {
        char strMessage[255]  = "";
        m_isLoaded            = false;
        m_isRedirected        = false;
        m_hinstDLL            = nullptr;
        m_pOrigFunctionAdress = nullptr;
        m_flOldProtect        = 0;
        m_myProtect           = PAGE_EXECUTE_READWRITE; // Protection settings on memory

        memset(m_oldInstructionsAsm, 0, sizeof(m_oldInstructionsAsm));
        memset(m_NewInstructionsAsmJmp, 0, sizeof(m_NewInstructionsAsmJmp));

        if (lpFileNameDll && lpNameFunctionToRedirect && newFunctionAdress)
        {
            m_hinstDLL = LoadLibraryA(lpFileNameDll);
            if (m_hinstDLL)
            {
                m_pOrigFunctionAdress = (LPVOID)GetProcAddress(m_hinstDLL, lpNameFunctionToRedirect);
                if (m_pOrigFunctionAdress)
                {
                    Redirect(newFunctionAdress);
                    m_isLoaded = true;
                }
                else
                {
                    sprintf_s(strMessage, sizeof(strMessage), "Error on get Adress [%s] on DLL [%s]\n", lpFileNameDll,
                              lpNameFunctionToRedirect);
                    OutputDebugStringA(strMessage);
                }
            }
            else
            {
                sprintf_s(strMessage, sizeof(strMessage), "Error on load DLL [%s]\n", lpFileNameDll);
                OutputDebugStringA(strMessage);
            }
        }
        else
        {
            sprintf_s(strMessage, sizeof(strMessage),
                      "All the args must be valid!!!\n Sample: (\"user32.dll\",\"MessageBoxA\",&MyMessageBoxA)\n");
            OutputDebugStringA(strMessage);
        }
    }

    REDIRECT_FUNCTION::~REDIRECT_FUNCTION()
    {
        SelectOriginalFunction();
        if (m_hinstDLL)
        {
            FreeLibrary(m_hinstDLL);
            m_hinstDLL = nullptr;
        }
    }

    void REDIRECT_FUNCTION::SelectOriginalFunction()
    {
        if (m_isRedirected && m_isLoaded)
        {
            VirtualProtect((LPVOID)m_pOrigFunctionAdress, SIZE_INSTRUCTIONS, m_myProtect, nullptr); // ReadWrite again
            memcpy(m_pOrigFunctionAdress, m_oldInstructionsAsm, SIZE_INSTRUCTIONS);              // Unhook API
            m_isRedirected = false;
        }
    }
    void REDIRECT_FUNCTION::SelectNewFunction()
    {
        if (!m_isRedirected && m_isLoaded)
        {
            memcpy(m_pOrigFunctionAdress, m_NewInstructionsAsmJmp, SIZE_INSTRUCTIONS);              // Rehook API
            VirtualProtect((LPVOID)m_pOrigFunctionAdress, SIZE_INSTRUCTIONS, m_flOldProtect, nullptr); // Normal setts
            m_isRedirected = true;
        }
    }

    void REDIRECT_FUNCTION::Redirect(LPVOID newFunction)
    {
        if (!m_isRedirected)
        {
            BYTE tempJMP[SIZE_INSTRUCTIONS] = {0xE9, 0x90, 0x90,
                                               0x90, 0x90, 0xC3};        // m_NewInstructionsAsmJmp <NOP> RET for now
            memcpy(m_NewInstructionsAsmJmp, tempJMP, SIZE_INSTRUCTIONS); // Copy into global for convenience later
            DWORD JMPSize = ((DWORD)newFunction - (DWORD)m_pOrigFunctionAdress - 5); // Get address difference
            VirtualProtect((LPVOID)m_pOrigFunctionAdress, SIZE_INSTRUCTIONS, PAGE_EXECUTE_READWRITE, &m_flOldProtect);
            // Change memory settings to make sure we can write the m_NewInstructionsAsmJmp in
            memcpy(m_oldInstructionsAsm, m_pOrigFunctionAdress,
                   SIZE_INSTRUCTIONS);                        // Copy old bytes before writing m_NewInstructionsAsmJmp
            memcpy(&m_NewInstructionsAsmJmp[1], &JMPSize, 4); // Write the address to m_NewInstructionsAsmJmp to
            memcpy(m_pOrigFunctionAdress, m_NewInstructionsAsmJmp, SIZE_INSTRUCTIONS); // Write it in process memory
            VirtualProtect((LPVOID)m_pOrigFunctionAdress, SIZE_INSTRUCTIONS, m_flOldProtect, nullptr); // Change setts back
        }
        m_isRedirected = true;
    }
    

