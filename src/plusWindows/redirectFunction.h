/*-----------------------------------------------------------------------------------------------------------------------|
| MIT License (MIT)                                                                                                      |
| Copyright (C) 2015      by Michel Braz de Morais  <michel.braz.morais@gmail.com>                                       |
|                                                                                                                        |
| Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated           |
| documentation files (the "Software"), to deal in the Software without restriction, including without limitation        |
| the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and       |
| to permit persons to whom the Software is furnished to do so, subject to the following conditions:                     |
|                                                                                                                        |
| The above copyright notice and this permission notice shall be included in all copies or substantial portions of       |
| the Software.                                                                                                          |
|                                                                                                                        |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE   |
| WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR  |
| COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       |
| OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.       |
|                                                                                                                        |
|-----------------------------------------------------------------------------------------------------------------------*/

#ifndef _REDIRECT_FUNCTION_H
#define _REDIRECT_FUNCTION_H

#ifndef NOMINMAX
    #define NOMINMAX
#endif

#include <Windows.h>
#include <stdio.h>

class REDIRECT_FUNCTION
{
  public:
    #define SIZE_INSTRUCTIONS 6 // Number of bytes needed to redirect

    REDIRECT_FUNCTION(const char *lpFileNameDll, const char *lpNameFunctionToRedirect, LPVOID newFunctionAdress);

    virtual ~REDIRECT_FUNCTION();

  public:
    void SelectOriginalFunction();
    void SelectNewFunction();

  private:
    void Redirect(LPVOID newFunction);
    LPVOID  m_pOrigFunctionAdress;
    BYTE    m_oldInstructionsAsm[SIZE_INSTRUCTIONS];    // This will hold the overwritten bytes
    BYTE    m_NewInstructionsAsmJmp[SIZE_INSTRUCTIONS]; // This holds the m_NewInstructionsAsmJmp to our code
    DWORD   m_flOldProtect;
    DWORD   m_myProtect; // Protection settings on memory
    bool    m_isLoaded;
    bool    m_isRedirected;
    HMODULE m_hinstDLL;
};

REDIRECT_FUNCTION *redirectGlobal = NULL;

#endif